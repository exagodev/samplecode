﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.Common;

	public partial class ServerEvents
	{
		// OnExceptionThrown
		// This server event is triggered whenever an exception is thrown in the web application.
		// This code demonstrates the usage of the OnExceptionThrown event.
		// This code is example code that must be modified for your environment and is not intended to be used as-is.
		public static bool ModifyExceptionLog(SessionInfo sessionInfo, params object[] args)
		{
			Exception exception = (Exception)args[0];
			Logger logger = (Logger)args[1];

			// EXAMPLE CODE start
			logger.ErrorFormat("User Id: {0}, Exception: {1}", sessionInfo.SetupData.Parameters.UserId, exception);
			return true; // Return true to prevent normal application logging for this exception.
			// EXAMPLE CODE end
		}
	}
}
