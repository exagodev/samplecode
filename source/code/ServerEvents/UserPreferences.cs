﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.SqlUtils;
	using WebReports.Api.Common;

	// This code contains two server events intended to be used in conjunction with each other.
	// These server events implement storage of user preferences in a SQLite database.
	// A SQLite file with the necessary schema is included in this repository.
	// Or set up a table with columns (userId [TEXT], companyId [TEXT], id [TEXT], value [TEXT])
	// This code is example code that must be modified for your environment and is not intended to be used as-is.

	public partial class ServerEvents
	{
		// OnSetUserPreference
		// Namespaces: WebReports.Api.SqlUtils
		public static void SetUserPreference(SessionInfo sessionInfo, string id, string value)
		{
			DbObjectFactory.DataSourceType sqlite = DbObjectFactory.DataSourceType.SQLite;
			Logger logger = Log.GetLogger();

			// EXAMPLE CODE start
			string databaseFilePath = "C:\\Exago\\UserPrefs.db";
			// EXAMPLE CODE end

			string connectionString = string.Format("Data Source={0};Version=3;", databaseFilePath);

			try
			{
				DbSqlHandler sql = new DbSqlHandler(sqlite, connectionString);

				if (string.IsNullOrEmpty(GetUserPreference(sessionInfo, id)))
					sql.Stmt = $"INSERT INTO prefs (userId, companyId, id, value) VALUES (@userId, @companyId, @id, @value);";

				else
					sql.Stmt = $"UPDATE prefs SET value = @value WHERE userId = @userId AND companyId = @companyId AND id = @id;";

				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@userId", sessionInfo.UserId));
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@companyId", sessionInfo.CompanyId));
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@id", id));
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@value", value));

				sql.ExecuteNQCmd();
			}
			catch (Exception e)
			{
				logger.ErrorFormat("Error: Set user preference failed with message {0}\n{1}", e.Message, e.StackTrace);
			}
		}

		// OnGetUserPreference
		// Namespaces: WebReports.Api.SqlUtils
		public static string GetUserPreference(SessionInfo sessionInfo, string id)
		{
			DbObjectFactory.DataSourceType sqlite = DbObjectFactory.DataSourceType.SQLite;
			Logger logger = Log.GetLogger();

			// EXAMPLE CODE start
			string databaseFilePath = "C:\\Exago\\UserPrefs.db";
			// EXAMPLE CODE end

			string connectionString = string.Format("Data Source={0};Version=3;", databaseFilePath);
			string retVal = null;

			try
			{
				DbSqlHandler sql = new DbSqlHandler(sqlite, connectionString);
				sql.Stmt = $"SELECT value FROM prefs WHERE userId = @userId AND companyId = @companyId AND id = @id;";
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@userId", sessionInfo.UserId));
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@companyId", sessionInfo.CompanyId));
				sql.SqlParams.Add(DbObjectFactory.GetParameter(sqlite, "@id", id));

				retVal = (string)sql.ExecuteScalarCmd();
			}
			catch (Exception e)
			{
				logger.ErrorFormat("Error: Get user preference failed with message {0}\n{1}", e.Message, e.StackTrace);
			}

			return retVal;
		}
	}
}
