﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.Reports;
	using WebReports.Api.Common;

	public partial class ServerEvents
	{
		// OnReportExecuteStart
		// This server event is triggered at the beginning of the report execution process.
		// This server event allows for multiple filter values to be passed to multi-value filters as parameters
		// with ~ delimited string values.
		// For example:
		//		IsOneOf filter value: @ParameterName@
		//		@ParameterName@ value: "Buchanan~Davolio~Callahan"
		// This code is fully functional and is intended to be used as-is.
		// Namespaces: WebReports.Api.Reports
		public static string PassMultipleValuesInParameter(SessionInfo sessionInfo)
		{
			string delim = "~";

			foreach (Parameter param in sessionInfo.SetupData.Parameters)
			{
				foreach (Filter filter in sessionInfo.Report.ExecFilters)
				{
					if (filter.Value == string.Format("@{0}@", param.Id))
						filter.DataValues.Clear();
					else
						continue;

					string[] values = param.Value.Split(new string[] { delim }, StringSplitOptions.None);

					foreach (string value in values)
						filter.AddValue(value);
				}
			}
			return null;
		}
	}
}
