﻿namespace WebReportsExtensions
{
    using WebReports.Api.Reports;
    using WebReports.Api.Common;

    public partial class ServerEvents
    {
		// OnReportExecuteStart
		// This server event is triggered at the beginning of the report execution process.
		// This server event dynamically appends a report footer to reports as they execute, containing text content.
		// This code is example code that must be modified for your environment and is not intended to be used as-is.
		// Namespaces: WebReports.Api.Reports
        public static string AppendDisclaimer(SessionInfo sessionInfo)
        {
            Row newRow = new Row(sessionInfo.PageInfo);
            newRow.SectionType = "Report Footer";
            sessionInfo.Report.Rows.Add(newRow);

            Cell newCell = new Cell(sessionInfo.PageInfo, newRow.Index, 0);

			// EXAMPLE CODE start
			newCell.SaveText = "this is a message";
			// EXAMPLE CODE end

            newCell.ColSpan = sessionInfo.Report.Columns.Count;
            sessionInfo.Report.Cells.Add(newCell);
            sessionInfo.Report.ReprocessAfterAlteration();
            return null;
        }
    }
}
