﻿namespace WebReportsExtensions
{
	using WebReports.Api.Common;
	using WebReports.Api.Programmability;

	public partial class ActionEvents
	{
		// Global event: OnDoubleClickReport
		// Double clicking on a report in the report tree causes this event to trigger
		// This code is fully functional and is intended to be used as-is.
		public static JavascriptAction DoubleClickToRun(SessionInfo sessionInfo, params object[] args)
		{
			string jscode = @"
				(function () {
					var node = clientInfo.utilities.TreeCtrl.GetSelectedNode(clientInfo.webReportsCtrl.reportsTreeId);
					if (!node)
						return false;
					clientInfo.ExecuteReport(node.reportName);
					return true;
				}());";

			sessionInfo.JavascriptAction.SetJsCode(jscode);
			return sessionInfo.JavascriptAction;
		}
	}
}
