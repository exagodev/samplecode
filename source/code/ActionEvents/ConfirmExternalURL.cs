﻿namespace WebReportsExtensions
{
	using WebReports.Api.Common;
	using WebReports.Api.Programmability;

	public partial class ActionEvents
	{
		// Global event: OnClassifyUserInputURL
		// v2018.2.6+
		// URLs that are loaded into or clicked within an Exago session cause this event to trigger
		// Return "internal" to allow the URL to load
		// Return "external" to prompt the user with a confirmation dialog before the URL will load
		// This code is example code that must be modified for your environment and is not intended to be used as-is.
		public static JavascriptAction ConfirmExternalURL(SessionInfo sessionInfo, params object[] args)
		{
			string jscode = @"
				(function (url) {

					// EXAMPLE CODE start
					if (url.includes(""exagoinc.com""))
						return ""internal"";
					else
						return ""external"";
					// EXAMPLE CODE end

				}(clientInfo.urlToClassify));";

			sessionInfo.JavascriptAction.SetJsCode(jscode);
			return sessionInfo.JavascriptAction;
		}
	}
}
