﻿namespace WebReportsExtensions
{
	using System;
	using System.Linq;
	using WebReports.Api.Common;

	public partial class CustomFunctions
	{
		// This function defines a series of numeric ranges and then displays which range the input value falls into.
		// This code is fully functional and is intended to be used as-is.
		// Return Type: String
		// References: System.Core.dll
		// Namespaces: System.Linq
		/* Usage
			 Range(Value, ShowFullRange, Endpoints...)
				•	Value - the input number
				•	ShowFullRange ( True() | False() )
					o	True() - show the full range, e.g. "100-200"
					o	False() - show the top end of the range, e.g. "200"
				•	Endpoints... ( Range1, Range2, ... ) - one or more range endpoints, in order from least to greatest
		*/
		public static object Range(SessionInfo sessionInfo, params object[] args)
		{
			double numVal;
			bool isNum = double.TryParse(args[0].ToString(), out numVal);
			bool isDisplay = bool.Parse(args[1].ToString());

			if (isNum)
			{
				if (numVal < (double)args[2])
					return isDisplay
						? "&#60; " + args[2]
						: args[2];

				for (int i = 3; i < args.Length; i++)
				{
					if (numVal > (double)args[i])
						continue;
					else
						return isDisplay
							? args[i - 1] + " &#45; " + args[i]
							: args[i];
				}
				return isDisplay
					? args.Last() + " &#62;"
					: args.Last();
			}
			return "Error: The first parameter must be a number";
		}
	}
}
