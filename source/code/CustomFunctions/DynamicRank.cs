﻿namespace WebReportsExtensions
{
	using System;
	using WebReports.Api.Common;
	using WebReports.Api.Reports;

	public partial class CustomFunctions
	{
		// This function displays an image whose path is dependent on an input value between 0 and 5
		// This code is example code that must be modified for your environment and is not intended to be used as-is.
		// Namespaces: WebReports.Api.Reports
		/*
		 * Usage
		    Rank(value)
			•	value is a number between 0 and 5. The number is rounded to the nearest integer and is supplied as
				a part of the file path to be loaded.
		    */
		public static object DynamicRank(SessionInfo sessionInfo, params object[] args)
		{
			double rank;
			if (!double.TryParse(args[0].ToString(), out rank) || 0 > rank || rank > 5)
				return "Error: Invalid input";

			// EXAMPLE CODE start
			string imagePath = "rank_" + (Math.Round(rank * 2) / 2).ToString() + ".png";
			// EXAMPLE CODE end

			string formulaText = string.Format("=LoadImage(\"{0}\")", imagePath);
			CellFormula formula = CellFormula.CreateFormula(sessionInfo.PageInfo, formulaText, CellVariableCollectionFilter.DataField);
			return formula.Evaluate(null);
		}
	}
}
